import 'package:flutter/material.dart';
import 'package:league_champions/utils/constants.dart';
import 'package:league_champions/services/champion_service.dart';


class Champion extends ChangeNotifier {
    // It's a good practice to put final in front of stuff that won't change
    final String id;
    final String name;
    final String title;
    final String blurb;
    final String image;
    List<ChampionSkin> skins;

    final ChampionService _championService = ChampionService();

    Champion({this.id, this.name, this.title, this.blurb, this.image});

    factory Champion.fromJson(Map<String, dynamic> json) {
        return Champion(
            id: json['id'],
            name: json['name'],
            title: json['title'],
            image: '${Constants.BaseChampionImagePath}${json['image']['full'] ?? ''}',
            blurb: json['blurb'],
        );
    }

    Future<List<ChampionSkin>> updateSkins() async {
        if (skins == null) {
        skins = await _championService.fetchChampionSkins(id);
        notifyListeners();
        }

        return skins;
    }

    @override
    String toString() {
        String toReturn = 'Name: ' + this.name ;
        toReturn += '\n\timage:' + this.image;
        toReturn += '\n\ttitle:' + this.title;
        toReturn += '\n\tblurb:' + this.blurb;
        toReturn += '\n\tid:' + this.id;

        return toReturn;
    }
}

class ChampionSkin {
  final String champion;
  final String id;
  final String name;
  final int num;
  final String splashImage;
  final String loadingImage;

  ChampionSkin({this.champion, this.id, this.name, this.num, this.splashImage, this.loadingImage});

  factory ChampionSkin.fromJson(Map<String, dynamic> json) {
    return ChampionSkin(
      id: json['id'],
      num: json['num'],
      name: json['name'],
      splashImage: '${Constants.BaseSplashImagePath}${json['champion']}_${json['num']}.jpg',
      loadingImage: '${Constants.BaseLoadingImagePath}${json['champion']}_${json['num']}.jpg',
    );
  }
}