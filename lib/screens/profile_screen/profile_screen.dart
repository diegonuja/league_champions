import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:league_champions/screens/profile_screen/local_widgets/profile_header.dart';
import 'package:league_champions/widgets/skins_gallery.dart';

class ProfileScreen extends StatelessWidget {
  static const routeName = '/profile';

  @override
  Widget build(BuildContext context) {
    final ProfileScreenArguments args = ModalRoute.of(context).settings.arguments;

    final Champion champion = args.champion;
    final String heroPrefix = args.heroPrefix;

    return Scaffold(
        appBar: AppBar(),
        body: Container(
            padding: EdgeInsets.only(top: 12, left: 20, right: 20),
            child: Column(
              children: <Widget>[
                ProfileHeader(champion: champion, heroPrefix: heroPrefix),
                Padding(
                  padding: EdgeInsets.only(top: 32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(bottom: 12),
                          child: Text('Skins',
                              textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
                      SkinsGallery(size: 320, champion: champion),
                    ],
                  ),
                )
              ],
            )));
  }
}

class ProfileScreenArguments {
  final Champion champion;
  final String heroPrefix;

  ProfileScreenArguments(this.champion, this.heroPrefix);
}
