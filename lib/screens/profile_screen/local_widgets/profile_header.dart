import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:league_champions/widgets/profile_picture.dart';

class ProfileHeader extends StatelessWidget {
  final Champion champion;
  final String heroPrefix;

  const ProfileHeader({this.champion, this.heroPrefix});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Hero(tag: '${heroPrefix}_${champion.id}', child: ProfilePicture(url: champion.image, size: 100)),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(champion.name, style: TextStyle(fontSize: 24, height: 1.6, fontWeight: FontWeight.bold)),
                Text(champion.title, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500))
              ],
            ),
          ),
        )
      ],
    );
  }
}
