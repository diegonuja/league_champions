
import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:league_champions/screens/profile_screen/profile_screen.dart';
import 'package:league_champions/widgets/profile_picture.dart';

class ChampionHorizontalSlider extends StatelessWidget {
    final List<Champion> champions;

    ChampionHorizontalSlider({ this.champions });

    @override
    Widget build(BuildContext context) {
        return ConstrainedBox(
            constraints: BoxConstraints(maxHeight: 100),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: () {
                            Navigator.pushNamed(context, ProfileScreen.routeName, arguments: ProfileScreenArguments(this.champions[index], 'champion_horizontal-slider'));
                        },
                        child: Container(
                            width: 88,
                            padding: EdgeInsets.only(left: 4, right: 4, top: 4),
                            child: Column(
                                children: [
                                    Hero(
                                        tag: 'champion_horizontal-slider_${champions[index].id}',
                                        child: ProfilePicture(url: champions[index].image, size: 60)
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Text(champions[index].name, style: TextStyle( fontSize: 12.0, fontWeight: FontWeight.bold) ,textAlign: TextAlign.center,)
                                    )
                                ],
                            )
                        )
                    );
                },
                itemCount: champions.length,
            )
        );
    }

}