
import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:league_champions/screens/champions_screen/local_widgets/champion_horizontal_slider.dart';
import 'package:league_champions/widgets/champion_feed.dart';
import 'package:provider/provider.dart';

class ChampionsScreen extends StatelessWidget {

    @override
    Widget build(BuildContext context) {

        List<Champion> champions = Provider.of<List<Champion>>(context);

        return Scaffold(
            appBar: AppBar(
                leading: IconButton(
                    icon: Image.asset('assets/images/league-golden-logo.png', width: 28, height: 28),
                    onPressed: () {  }
                ),
                title: Text('Champions', style: TextStyle( fontSize: 17.0,),)
            ),
            body: (champions == null || champions.length == 0)
            ? Container(
                child: Row(
                    children: [CircularProgressIndicator()],
                    mainAxisAlignment: MainAxisAlignment.center,
                )
            )
            : Container(
                child: Column(
                    children: <Widget>[
                        ChampionHorizontalSlider(champions: champions),
                        Expanded(
                            child: ListView.builder(
                            itemCount: champions.length,
                            itemBuilder: (context, index) {
                                return ChampionFeed(champion: champions[index],);
                            })
                        )
                    ],
                ),
            )
        );
    }
}