import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class ProfilePicture extends StatelessWidget {
  final String url;
  final double size;

  ProfilePicture({this.url, this.size});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.size,
      height: this.size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        border: Border.all(width: 2, color: Theme.of(context).accentColor),
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [Theme.of(context).primaryColor, Theme.of(context).accentColor]),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: FadeInImage.memoryNetwork(
          image: this.url,
          placeholder: kTransparentImage,
          fadeInDuration: Duration(milliseconds: 200),
        ),
      ),
    );
  }
}
