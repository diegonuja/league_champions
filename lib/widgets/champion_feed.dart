import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:league_champions/screens/profile_screen/profile_screen.dart';
import 'package:league_champions/widgets/profile_picture.dart';
import 'package:league_champions/widgets/skins_gallery.dart';
import 'package:provider/provider.dart';

class ChampionFeed extends StatelessWidget {
  final Champion champion;

  ChampionFeed({this.champion});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: this.champion,
        child: Consumer<Champion>(
          builder: (context, champion, child) {
            return Container(
                padding: EdgeInsets.symmetric(vertical: 12),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: GestureDetector(
                          onTap: () => {
                                Navigator.pushNamed(context, ProfileScreen.routeName,
                                    arguments: ProfileScreenArguments(champion, 'champion_feed'))
                              },
                          child: Row(
                            children: <Widget>[
                              Hero(
                                  tag: 'champion_feed_${this.champion.id}',
                                  child: ProfilePicture(
                                    url: this.champion.image,
                                    size: 38,
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(left: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(this.champion.name,
                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                                      Text(this.champion.title,
                                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
                                    ],
                                  ))
                            ],
                          )),
                    ),
                    Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: SkinsGallery(size: 300, champion: this.champion)),
                  ],
                ));
          },
        ));
  }
}
