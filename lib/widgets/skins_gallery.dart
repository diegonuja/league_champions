import 'package:flutter/material.dart';
import 'package:league_champions/models/champion.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class SkinsGallery extends StatefulWidget {
  final Champion champion;
  final double size;

  SkinsGallery({this.champion, this.size});

  _SkinsGalleryState createState() => _SkinsGalleryState();
}

class _SkinsGalleryState extends State<SkinsGallery> {
  int currentPage = 1;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onPageViewChange(int page) {
    setState(() {
      this.currentPage = page + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (this.widget.champion.skins == null) this.widget.champion.updateSkins();

    return ChangeNotifierProvider.value(
      value: this.widget.champion,
      child: Consumer<Champion>(builder: (context, champion, child) {
        return Column(
          children: <Widget>[
            Container(
              height: this.widget.size,
              width: double.infinity,
              child: (champion.skins == null)
                  ? Container(alignment: Alignment.center, child: CircularProgressIndicator())
                  : Stack(
                      children: <Widget>[
                        PageView.builder(
                          onPageChanged: _onPageViewChange,
                          itemCount: champion.skins.length,
                          allowImplicitScrolling: true,
                          itemBuilder: (context, index) {
                            return Stack(
                              alignment: Alignment.bottomCenter,
                              children: <Widget>[
                                Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                            colors: [Theme.of(context).primaryColor, Theme.of(context).accentColor])),
                                    child: ClipRRect(
                                        child: FadeInImage.memoryNetwork(
                                      image: champion.skins[index].splashImage,
                                      height: this.widget.size,
                                      fit: BoxFit.cover,
                                      placeholder: kTransparentImage,
                                      fadeInDuration: Duration(milliseconds: 200),
                                    ))),
                                Container(
                                    color: Colors.black54,
                                    width: double.infinity,
                                    padding: EdgeInsets.symmetric(vertical: 4),
                                    child: Text(
                                      champion.skins[index].name,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    )),
                              ],
                            );
                          },
                        ),
                        Positioned(
                          top: 16,
                          right: 16,
                          child: Container(
                              padding: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.black54,
                              ),
                              child: Text(
                                '${this.currentPage}/${champion.skins.length}',
                                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                              )),
                        )
                      ],
                    ),
            ),
            if (champion.skins != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  for (int i = 0; i < champion.skins.length; i++)
                    Padding(
                      padding: EdgeInsets.only(top: 12, right: 2, left: 2),
                      child: Container(
                        width: (this.currentPage == i + 1) ? 6 : 4,
                        height: (this.currentPage == i + 1) ? 6 : 4,
                        decoration: new BoxDecoration(
                          color: (this.currentPage == i + 1) ? Theme.of(context).accentColor : Colors.grey,
                          shape: BoxShape.circle,
                        ),
                      ),
                    )
                ],
              )
          ],
        );
      }),
    );
  }
}
