import 'package:flutter/material.dart';
import 'package:league_champions/screens/champions_screen/champions_screen.dart';
import 'package:league_champions/screens/profile_screen/profile_screen.dart';
import 'package:league_champions/services/champion_service.dart';
import 'package:league_champions/utils/constants.dart';
import 'package:provider/provider.dart';

import 'models/champion.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final ChampionService _championService = ChampionService();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        FutureProvider<List<Champion>>(create: (context) => _championService.getChampions()),
      ],
      child: MaterialApp(
        title: 'League of Legends Champions',
        theme: Constants.lightTheme,
        darkTheme: Constants.darkTheme,
        initialRoute: '/',
        routes: {
          '/': (context) => ChampionsScreen(),
          '/profile': (context) => ProfileScreen(),
        },
      ),
    );
  }
}
